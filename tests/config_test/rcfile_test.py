import errno
import os

import pytest

from stig.config import defaults, rcfile

RCFILE_CONTENTS = r"""
# this is a comment
foo bar baz

foo

# demonstration of line break escaping
foo \
 # Comment within command arguments
 bar\
        # Comment within command chain
	&	baz   	\
# Quoted space
"b a m"
  escaped\ space  

# the end
""".strip()  # noqa

def test_read_file_successfully(tmp_path):
    filepath = tmp_path / 'rcfile'
    filepath.write_text(RCFILE_CONTENTS)
    lines = rcfile.read(str(filepath))
    exp_numbered_lines = (
        (2, (('foo', 'bar', 'baz'),)),
        (4, (('foo',),)),
        (7, (('foo', 'bar'), '&', ('baz', 'b a m'))),
        (14, (('escaped space',),)),
    )
    for line, (exp_number, exp_line) in zip(lines, exp_numbered_lines):
        assert line == exp_line
        assert line.number == exp_number


def test_default_file_not_found(mocker):
    mocker.patch.object(defaults, 'RC_FILE', '/nonexisting/path')
    filepath = defaults.RC_FILE
    cmds = rcfile.read(filepath)
    assert cmds == ()


def test_default_file_not_readable(mocker, tmp_path):
    filepath = tmp_path / 'rcfile'
    filepath.write_text(RCFILE_CONTENTS)
    filepath.chmod(0o000)
    mocker.patch.object(defaults, 'RC_FILE', str(filepath))
    exp_msg = os.strerror(errno.EACCES)
    try:
        with pytest.raises(rcfile.RCFileError, match=rf'^Failed to read {filepath}: {exp_msg}$'):
            rcfile.read(str(filepath))
    finally:
        filepath.chmod(0o600)


def test_custom_file_not_found(mocker):
    filepath = '/my/special/nonexisting/path'
    with pytest.raises(rcfile.RCFileError, match=rf'^No such file: {filepath}$'):
        rcfile.read(filepath)


def test_custom_file_not_readable(mocker, tmp_path):
    filepath = tmp_path / 'rcfile'
    filepath.write_text(RCFILE_CONTENTS)
    filepath.chmod(0o000)
    exp_msg = os.strerror(errno.EACCES)
    try:
        with pytest.raises(rcfile.RCFileError, match=rf'^Failed to read {filepath}: {exp_msg}$'):
            rcfile.read(str(filepath))
    finally:
        filepath.chmod(0o600)
