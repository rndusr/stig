import os

import pytest

from stig import utils

USER = os.environ['USER']
HOME = os.environ['HOME']

@pytest.mark.parametrize(
    argnames='path, exp_return_value',
    argvalues=(
        ('foo', 'foo'),
        ('./foo/bar', './foo/bar'),
        ('/foo/bar', '/foo/bar'),
        (f'{HOME}/bar', '~/bar'),
        (f'~{USER}/bar', f'~{USER}/bar'),
        ('~/bar', '~/bar'),
        ('~/', '~/'),
        ('~', '~'),
    )
)
def test_tildify_path(path, exp_return_value):
    return_value = utils.tildify_path(path)
    assert return_value == exp_return_value
