import re

import pytest

from stig.command import _argv, _error


@pytest.mark.parametrize(
    argnames='args, exp_result',
    argvalues=(
        ([], _error.CommandError('Command cannot be empty: []')),
        (['foo'], ('foo',)),
        (['foo', '--bar'], ('foo', '--bar')),
    ),
    ids=lambda v: repr(v),
)
def test___init__(args, exp_result):
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            _argv.Argv(args)
    else:
        cmd = _argv.Argv(args)
        assert cmd == exp_result
