import pytest

from stig.command import _example


@pytest.mark.parametrize(
    argnames='command, description_gap, description, exp_string',
    argvalues=(
        (
            'foo this', 2, '',
            'foo this',
        ),
        (
            'foo this', 2, 'Foo something',
            'foo this  # Foo something',
        ),
        (
            'foo this', 6, 'Foo something',
            'foo this      # Foo something',
        ),
        (
            'foo this', 1, (
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, '
                'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. '
                'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris '
                'nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in '
                'reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla '
                'pariatur. Excepteur sint occaecat cupidatat non proident, sunt in '
                'culpa qui officia deserunt mollit anim id est laborum.'
            ),
            (
                'foo this # Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\n'
                '         # incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis\n'
                '         # nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n'
                '         # Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu\n'
                '         # fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in\n'
                '         # culpa qui officia deserunt mollit anim id est laborum.'
            ),
        ),
    ),
    ids=lambda v: repr(v),
)
def test_Example(command, description_gap, description, exp_string):
    return_value = _example.Example(
        command=command,
        description_gap=description_gap,
        description=description,
    )
    print(return_value)
    print(exp_string)
    assert return_value == exp_string

    assert return_value.command == command
    assert return_value.description == description
    assert return_value.description_gap == description_gap


@pytest.mark.parametrize(
    argnames='examples, exp_string',
    argvalues=(
        (
            (
                _example.Example(
                    command='foo',
                    description='Just foo',
                ),
                _example.Example(
                    command='foo bar',
                ),
                _example.Example(
                    command='foo with lots of arguments',
                    description=(
                        "Very long description of foo that should span multiple lines. "
                        "We might even have to scroll up to the beginning of this text, "
                        "but that's pretty unlikely unless our terminal is really small."
                    ),
                ),
            ),
            (
                'foo                         # Just foo',
                'foo bar',
                "foo with lots of arguments  # Very long description of foo that should span multiple\n"
                "                            # lines. We might even have to scroll up to the beginning of\n"
                "                            # this text, but that's pretty unlikely unless our terminal is\n"
                "                            # really small."
            ),
        ),
    ),
    ids=lambda v: repr(v),
)
def test_Examples(examples, exp_string):
    return_value = _example.Examples(*examples)
    print(return_value)
    print(exp_string)
    assert return_value == exp_string
