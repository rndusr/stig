import itertools
import re
from unittest.mock import call

import pytest

from stig.command import _cmdchain, _constants, _error


def test___str__():
    cmdchain = _cmdchain.CommandChain(
        ('foo', 'bar baz'),
        'OR',
        ('foo bar', 'baz'),
        'AND',
        ('foo', 'bar', 'baz'),
    )
    assert str(cmdchain) == (
        "foo 'bar baz' "
        "OR "
        "'foo bar' baz "
        "AND "
        "foo bar baz"
    )


@pytest.mark.parametrize('operator', _constants.OPERATORS)
@pytest.mark.parametrize(
    argnames='string, exp_result',
    argvalues=(
        ('foo', ['foo']),
        ('foo bar baz', ['foo', 'bar', 'baz']),
        ('foo "bar" baz', ['foo', 'bar', 'baz']),
        ('foo "bar baz"', ['foo', 'bar baz']),
        ('"foo bar baz"', ['foo bar baz']),
        ('"foo bar baz"', ['foo bar baz']),
        ('foo {OP} bar {OP} baz', ['foo', '{OP}', 'bar', '{OP}', 'baz']),
        ('foo{OP}bar{OP}baz', ['foo', '{OP}', 'bar', '{OP}', 'baz']),
        ('foo "bar baz', _error.CommandError('No closing quotation')),
        ("foo 'bar baz", _error.CommandError('No closing quotation')),
        (r"foo \'bar baz", ['foo', "'bar", 'baz']),
        (r'foo \"bar baz', ['foo', '"bar', 'baz']),
        (r'foo \{OP} bar', ['foo', '{OP}', 'bar']),
        (r'foo\{OP}bar', ['foo{OP}bar']),
        (r'"foo \{OP} bar"', [r'foo \{OP} bar']),
    ),
    ids=lambda v: str(v),
)
def test_from_string(string, operator, exp_result, mocker):
    mocker.patch.object(_cmdchain.CommandChain, 'from_args')

    string = string.format(OP=operator)
    if not isinstance(exp_result, Exception):
        exp_result = [
            arg.format(OP=operator)
            for arg in exp_result
        ]

    print('STRING:', string)
    print('EXP_ARGS:', exp_result)

    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            _cmdchain.CommandChain.from_string(string)
    else:
        cmdchain = _cmdchain.CommandChain.from_string(string)
        assert cmdchain is _cmdchain.CommandChain.from_args.return_value
        assert _cmdchain.CommandChain.from_args.call_args_list == [
            call(exp_result)
        ]


ops_cycle = itertools.cycle(_constants.OPERATORS)

@pytest.mark.parametrize('op1, op2', [
    (next(ops_cycle), next(ops_cycle))
    for _ in range(len(_constants.OPERATORS))
])
@pytest.mark.parametrize(
    argnames='args, exp_result',
    argvalues=(
        (
            [],
            [],
        ),
        (
            ['foo'],
            [
                ('foo',),
            ],
        ),
        (
            ['foo', 'bar', 'baz'],
            [
                ('foo', 'bar', 'baz'),
            ],
        ),
        (
            ['foo', '1', '2', '{OP1}', 'bar', '2', '3'],
            [
                ('foo', '1', '2'),
                '{OP1}',
                ('bar', '2', '3'),
            ],
        ),
        (
            ['foo', '1', '2', '{OP1}', 'bar', '2', '3', '{OP2}', 'baz'],
            [
                ('foo', '1', '2'),
                '{OP1}',
                ('bar', '2', '3'),
                '{OP2}',
                ('baz',),
            ],
        ),
        (
            ['foo', '1', '2', r'\{OP1}', '{OP2}', 'bar', '2', '3', r'\{OP2}', 'baz'],
            [
                ('foo', '1', '2', '{OP1}'),
                '{OP2}',
                ('bar', '2', '3', '{OP2}', 'baz'),
            ],
        ),
        (
            ['{OP1}', 'bar', '2', '3'],
            _error.CommandError('Command cannot start with operator: {OP1}'),
        ),
        (
            ['bar', '2', '3', '{OP1}'],
            _error.CommandError('Command cannot end with operator: {OP1}'),
        ),
        (
            ['foo', '{OP1}', '{OP2}', 'bar'],
            _error.CommandError('Consecutive operators: {OP1} {OP2}'),
        ),
        (
            ['foo', '1', '2', '{OP1}', '{OP2}', 'bar'],
            _error.CommandError('Consecutive operators: {OP1} {OP2}'),
        ),
    ),
    ids=lambda v: str(v),
)
def test_from_args(args, op1, op2, exp_result):
    args = [
        arg.format(OP1=op1, OP2=op2)
        for arg in args
    ]
    if not isinstance(exp_result, Exception):
        exp_result = tuple(
            (
                # item is normalized operator
                item.format(OP1=op1, OP2=op2)
                if isinstance(item, str) else
                # item is command
                tuple(
                    arg.format(OP1=op1, OP2=op2)
                    for arg in item
                )
            )
            for item in exp_result
        )

    print('ARGS:', args)
    print('EXP_CHAIN:', exp_result)

    if isinstance(exp_result, Exception):
        exp_msg = str(exp_result).format(OP1=op1, OP2=op2)
        with pytest.raises(type(exp_result), match=rf'^{re.escape(exp_msg)}$'):
            _cmdchain.CommandChain.from_args(args)
    else:
        chain = _cmdchain.CommandChain.from_args(args)
        import pprint
        pprint.pprint(chain, width=30)
        assert chain == exp_result
