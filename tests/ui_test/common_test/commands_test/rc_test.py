import os
from unittest.mock import patch

import pytest

from stig.config import defaults
from stig.ui.common import commands


@pytest.mark.parametrize(
    argnames='path, exists, exp_path',
    argvalues=(
        pytest.param(
            '/some/absolute/path', False, '/some/absolute/path',
            id='Absolute path exists=False',
        ),
        pytest.param(
            '/some/absolute/path', True, '/some/absolute/path',
            id='Absolute path exists=True',
        ),
        pytest.param(
            '~/home/path', False, os.path.expanduser('~/home/path'),
            id='Home path exists=False',
        ),
        pytest.param(
            '~/home/path', True, os.path.expanduser('~/home/path'),
            id='Home path exists=True',
        ),
        pytest.param(
            '~user/path', False, os.path.expanduser('~user/path'),
            id='User path exists=False',
        ),
        pytest.param(
            '~user/path', True, os.path.expanduser('~user/path'),
            id='User path exists=True',
        ),
        pytest.param(
            './relative/path', False, './relative/path',
            id='Explicit relative path exists=False',
        ),
        pytest.param(
            './relative/path', True, './relative/path',
            id='Explicit relative path exists=True',
        ),
        pytest.param(
            'relative/path', False, f'{os.path.dirname(defaults.RC_FILE)}/relative/path',
            id='Relative path exists=False',
        ),
        pytest.param(
            'relative/path', True, 'relative/path',
            id='Relative path exists=True',
        ),
        pytest.param(
            'filename', False, f'{os.path.dirname(defaults.RC_FILE)}/filename',
            id='File name exists=False',
        ),
        pytest.param(
            'filename', True, 'filename',
            id='File name exists=True',
        ),
    ),
)
def test_get_rc_filepath(path, exists, exp_path):
    with patch('os.path.exists', return_value=exists):
        return_value = commands.Rc.get_rc_filepath(path)
    assert return_value == exp_path
