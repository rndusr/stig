import itertools
import re

import prompt_toolkit as ptk
import pytest

from stig.ui.tui2 import _key

EXP_UNSUPPORTED_KEY_COMBINATIONS = {
    ('Enter', ('Ctrl',)),
    ('Enter', ('Shift',)),
    ('Enter', ('Ctrl', 'Shift')),

    ('Escape', ('Ctrl',)),
    ('Escape', ('Ctrl', 'Shift')),

    ('Tab', ('Ctrl',)),
    ('Tab', ('Ctrl', 'Shift')),

    ('Space', ('Shift',)),
    ('Space', ('Ctrl', 'Shift')),

    ('Backspace', ('Ctrl',)),
    ('Backspace', ('Shift',)),
    ('Backspace', ('Ctrl', 'Shift')),

    ('Insert', ('Ctrl',)),
    ('Insert', ('Shift',)),
    ('Insert', ('Ctrl', 'Shift')),

    ('F1', ('Shift',)),
    ('F2', ('Shift',)),
    ('F3', ('Shift',)),
    ('F4', ('Shift',)),
    ('F5', ('Shift',)),
    ('F6', ('Shift',)),
    ('F7', ('Shift',)),
    ('F8', ('Shift',)),
    ('F9', ('Shift',)),
    ('F10', ('Shift',)),
    ('F11', ('Shift',)),
    ('F12', ('Shift',)),
    ('F13', ('Shift',)),
    ('F14', ('Shift',)),
    ('F15', ('Shift',)),
    ('F16', ('Shift',)),
    ('F17', ('Shift',)),
    ('F18', ('Shift',)),
    ('F19', ('Shift',)),
    ('F20', ('Shift',)),
    ('F21', ('Shift',)),
    ('F22', ('Shift',)),
    ('F23', ('Shift',)),
    ('F24', ('Shift',)),

    ('F1', ('Ctrl', 'Shift')),
    ('F2', ('Ctrl', 'Shift')),
    ('F3', ('Ctrl', 'Shift')),
    ('F4', ('Ctrl', 'Shift')),
    ('F5', ('Ctrl', 'Shift')),
    ('F6', ('Ctrl', 'Shift')),
    ('F7', ('Ctrl', 'Shift')),
    ('F8', ('Ctrl', 'Shift')),
    ('F9', ('Ctrl', 'Shift')),
    ('F10', ('Ctrl', 'Shift')),
    ('F11', ('Ctrl', 'Shift')),
    ('F12', ('Ctrl', 'Shift')),
    ('F13', ('Ctrl', 'Shift')),
    ('F14', ('Ctrl', 'Shift')),
    ('F15', ('Ctrl', 'Shift')),
    ('F16', ('Ctrl', 'Shift')),
    ('F17', ('Ctrl', 'Shift')),
    ('F18', ('Ctrl', 'Shift')),
    ('F19', ('Ctrl', 'Shift')),
    ('F20', ('Ctrl', 'Shift')),
    ('F21', ('Ctrl', 'Shift')),
    ('F22', ('Ctrl', 'Shift')),
    ('F23', ('Ctrl', 'Shift')),
    ('F24', ('Ctrl', 'Shift')),

    ('F3', ('Ctrl',)),
    ('F15', ('Ctrl',)),
}

# Every unsupported key combination is also unsupported with the Alt modifier
EXP_UNSUPPORTED_KEY_COMBINATIONS.update(
    (key, ('Alt',) + modifiers)
    for key, modifiers in tuple(EXP_UNSUPPORTED_KEY_COMBINATIONS)
)


def parametrize_key_combinations():
    # Each combination of modifiers for each possible number of modifiers
    modifier_permutations = (
        modifiers
        for r in range(0, len(_key.Key._MODIFIERS) + 1)
        for modifiers in itertools.permutations(_key.Key._MODIFIERS, r=r)
    )

    for modifiers in modifier_permutations:
        for key in _key.Key._SPECIAL_KEYS:
            if modifiers:
                exp_string = '-'.join(sorted(modifiers)) + '-' + key
            else:
                exp_string = key
            yield key, modifiers, exp_string

@pytest.mark.parametrize(
    argnames='key, modifiers, exp_string',
    argvalues=parametrize_key_combinations(),
    ids=lambda v: str(v),
)
def test_instantiation_with_parametrized_argument(key, modifiers, exp_string):
    # Some keys are technically impossible, e.g. <Ctrl-Enter>
    if (key, tuple(sorted(modifiers))) in EXP_UNSUPPORTED_KEY_COMBINATIONS:
        unsupported_key = _key.Key(key, modifiers, validate=False)
        match = rf'^Unsupported key combination: {re.escape(str(unsupported_key))}$'
        with pytest.raises(ValueError, match=match):
            _key.Key(key, modifiers=modifiers)
    else:
        string = _key.Key(key, modifiers=modifiers)
        assert string == exp_string


@pytest.mark.parametrize(
    argnames='from_key, from_modifiers, exp_key',
    argvalues=(
        (
            from_key,
            from_modifiers,
            _key.Key(to_key, to_modifiers, validate=False),
        )
        for (from_key, from_modifiers), (to_key, to_modifiers) in _key.Key._COMBINATION_TRANSLATION.items()
    ),
    ids=lambda v: str(v),
)
def test_instantiation_with_translated_combination(from_key, from_modifiers, exp_key):
    string = _key.Key(from_key, modifiers=from_modifiers)
    assert string == exp_key


@pytest.mark.parametrize(
    argnames='key, modifiers, exp_result',
    argvalues=(
        ('á', (), 'á'),
        ('Д', ('Alt',), 'Alt-Д'),
        ('λ', ('Ctrl',), ValueError('Unsupported key combination: <Ctrl-λ>')),
        ('ᵹ', ('Ctrl', 'Alt'), ValueError('Unsupported key combination: <Alt-Ctrl-ᵹ>')),
        ('ท', ('Ctrl', 'Shift'), ValueError('Unsupported key combination: <Ctrl-Shift-ท>')),
    ),
    ids=lambda v: str(v),
)
def test_instantiation_with_custom_argument(key, modifiers, exp_result):
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            _key.Key(key, modifiers=modifiers)
    else:
        string = _key.Key(key, modifiers=modifiers)
        assert string == exp_result


@pytest.mark.parametrize(
    argnames='key, modifiers, exp_exception',
    argvalues=(
        ('', (), ValueError("Key must not be empty")),
        ('foo', (), ValueError('Invalid key: foo')),
        ('foo', ('Ctrl',), ValueError('Invalid key: foo')),
        ('x', ('foo',), ValueError('Invalid modifier: foo')),
        ('x', ('Ctrl', 'foo',), ValueError('Invalid modifier: foo')),
        ('x', ('Alt', 'Ctrl', 'Alt'), ValueError('Duplicate modifier: Alt')),
    ),
    ids=lambda v: str(v),
)
def test_instantiation_with_invalid_argument(key, modifiers, exp_exception):
    with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
        _key.Key(key, modifiers=modifiers)


def test___hash__():
    key = _key.Key('x', modifiers=('Ctrl',))
    assert hash(key) is not None


@pytest.mark.parametrize(
    argnames='key, modifiers, exp_repr',
    argvalues=(
        ('x', (), "Key('x', modifiers=())"),
        ('x', ('Ctrl',), "Key('x', modifiers=('Ctrl',))"),
        ('Home', ('Shift', 'Ctrl',), "Key('Home', modifiers=('Ctrl', 'Shift'))"),
        ('Home', ('Shift', 'Alt',), "Key('Home', modifiers=('Alt', 'Shift'))"),
    ),
    ids=lambda v: str(v),
)
def test___repr__(key, modifiers, exp_repr):
    key = _key.Key(key, modifiers=modifiers)
    assert repr(key) == exp_repr


def _parametrize_ptk_key_values():
    # Keys we don't mess with
    ignored_ptk_keys = (
        ptk.keys.Keys.ScrollUp,
        ptk.keys.Keys.ScrollDown,
        ptk.keys.Keys.CPRResponse,
        ptk.keys.Keys.Vt100MouseEvent,
        ptk.keys.Keys.WindowsMouseEvent,
        ptk.keys.Keys.BracketedPaste,
        ptk.keys.Keys.SIGINT,
        ptk.keys.Keys.Ignore,
    )
    for ptk_key in ptk.input.ansi_escape_sequences.REVERSE_ANSI_SEQUENCES:
        if ptk_key not in ignored_ptk_keys:
            yield ptk_key.value

@pytest.mark.parametrize(
    argnames='ptk_key_value',
    argvalues=_parametrize_ptk_key_values(),
    ids=lambda v: str(v),
)
def test_ptk_value_transparency(ptk_key_value):
    # Translate ptk sequence to our own format and back again
    key = _key.Key.from_ptk_value(ptk_key_value)
    assert key.as_ptk_value == ptk_key_value

@pytest.mark.parametrize(
    argnames='ptk_key_value',
    argvalues=(
        'x',
        'X',
        '%',
        'á',
    ),
    ids=lambda v: str(v),
)
def test_ptk_value_transparency_with_custom_value(ptk_key_value):
    # Translate ptk key to our own format and back again
    key = _key.Key.from_ptk_value(ptk_key_value)
    assert key.as_ptk_value == ptk_key_value
