import importlib
import inspect
import os

from .. import log


def tildify_path(path):
    """
    Replace home directory in `path` with "~"
    """
    homedir = os.path.expanduser('~')
    if path.startswith(homedir):
        return '~' + path[len(homedir):]
    return path


def find_subclasses(*, basecls, module_names):
    """Yield subclasses of `basecls` in `module_names`"""

    def is_subcls(obj):
        return (
            isinstance(obj, type)
            and obj is not basecls
            and issubclass(obj, basecls)
        )

    for module_name in module_names:
        log.debug('Finding %s subclasses in %s', basecls.__name__, module_name)
        module = importlib.import_module(module_name)
        members = inspect.getmembers(module, predicate=is_subcls)
        for subclsname, subcls in members:
            yield subcls
