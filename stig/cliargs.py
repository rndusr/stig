import argparse
import sys

from . import command, config, ui, utils


def parse(argv):
    parser = argparse.ArgumentParser(add_help=False)
    descriptions = {}

    def add_arg(*names, section, description, varname=None, **kwargs):
        option_string = ', '.join(names)
        if varname is not None:
            option_string += f' [{varname}]'

        if section not in descriptions:
            descriptions[section] = {}

        descriptions[section][option_string] = description

        parser.add_argument(*names, **kwargs)

    add_arg(
        '--help', '-h',
        nargs='*',
        default=None,
        varname='TOPIC',
        section='OPTIONS',
        description="Display help about TOPIC",
    )

    add_arg(
        '--version', '-v',
        action='store_true',
        section='OPTIONS',
        description="Display version number and exit",
    )

    add_arg(
        '--ui', '-u',
        choices=ui.ui_names(),
        default=None,
        varname='UI',
        section='OPTIONS',
        description='Force user interface instead of guessing',
    )

    add_arg(
        '--no-default-rc-file', '--nodefaultrcfile', '-R', action='store_true',
        section='OPTIONS',
        description=(
            'Do not run commands from default rc file'
            f' ({utils.tildify_path(config.defaults.RC_FILE)})'
        ),
    )

    add_arg(
        '--debug',
        type=lambda mods: mods.split(','),
        default=(),
        varname='MODULES',
        section='DEVELOPER OPTIONS',
        description=(
            'Log debug messages from comma-separated list of MODULES'
            ' (e.g. "config.settings,commands")'
        ),
    )

    add_arg(
        '--debug-file', '--debugfile',
        varname='FILE',
        section='DEVELOPER OPTIONS',
        description='Log debug messages to FILE',
    )

    # Anything not specified above is a chain of subcommands
    parser.add_argument('subcmds', nargs=argparse.REMAINDER)
    args = vars(parser.parse_args(argv))
    subcmds = args.pop('subcmds')

    # Convert -h option to 'help' command
    if args['help']:
        subcmds.append('help')
        subcmds.extend(args['help'])

    # Convert -v option to 'version' command
    if args['version']:
        subcmds.append('version')

    # Get CommandChain from `subcmds`
    try:
        cli_command_chain = command.CommandChain.from_args(subcmds)
    except command.CommandError as e:
        # argparse.ArgumentParser exits on failure, so we do the same
        sys.stderr.write(f'{e}\n')
        sys.exit(1)
    else:
        return (
            # Global CLI arguments
            args,
            # Any unrecognized arguments are subcommands
            cli_command_chain,
        )
