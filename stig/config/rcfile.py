import os

from .. import command
from . import defaults


class RCFileError(Exception):
    """Raised by :func:`.rcfile.read`"""
    pass


class RCCommandChain(command.CommandChain):
    """
    :class:`~.CommandChain` subclass with a line `number` argument and
    attribute
    """

    @classmethod
    def from_rcline(cls, number, line):
        self = cls.from_string(line)
        self._number = number
        return self

    @property
    def number(self):
        return self._number


def read(filepath):
    """
    Read sequence of :class:`RCCommandChain` instances from `filepath`

    Lines that start with "#" are ignored.

    Line breaks can be escaped with a backslash.
    """
    filepath = os.path.expanduser(filepath)
    try:
        with open(filepath, 'r') as f:
            lines = f.readlines()

    except FileNotFoundError:
        if filepath == defaults.RC_FILE:
            # Missing default rc file is not an error
            lines = ()
        else:
            raise RCFileError(f'No such file: {filepath}')

    except OSError as e:
        msg = e.strerror if e.strerror else str(e)
        raise RCFileError(f'Failed to read {filepath}: {msg}')

    return tuple(
        RCCommandChain.from_rcline(number, line)
        for number, line in _concatenated(
            _ignored(
                _numbered(
                    _stripped(lines)
                )
            )
        )
    )


def _stripped(lines):
    for line in lines:
        yield line.strip()


def _numbered(lines):
    for number, line in enumerate(lines, start=1):
        yield number, line


def _ignored(numbered_lines):
    for number, line in numbered_lines:
        if line and not line.startswith('#'):
            yield number, line


def _concatenated(numbered_lines):
    current_line = []
    current_number = None
    for number, line in numbered_lines:
        if line[-1] == '\\':
            # Remember the first line of a multiline
            if current_number is None:
                current_number = number
            current_line.append(line[:-1].rstrip(' \t'))
        else:
            current_line.append(line)
            # Report the first line of a multiline or the regular line number
            if current_number is None:
                current_number = number
            yield current_number, ' '.join(current_line)
            current_line.clear()
            current_number = None
