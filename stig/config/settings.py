import collections


class Settings(collections.abc.Mapping):
    """
    User configuration as immutable mapping

    Values are either stored internally in a :class:`dict` or managed via
    getter/setter functions which are called whenever a value is requested or
    changed.
    """

    def __init__(self):
        self._defaults = {}
        self._values = {}
        self._constructors = {}
        self._getters = {}
        self._setters = {}
        self._descriptions = {}

    def __repr__(self):
        return f'<Settings {self._values!r}>'

    def __getitem__(self, name):
        getter = self._getters.get(name)
        if getter is not None:
            return self._validate(name, getter())
        else:
            return self._values[name]

    def __setitem__(self, name, value):
        validated_value = self._validate(name, value)
        setter = self._setters.get(name, None)
        if setter is not None:
            setter(validated_value)
        else:
            self._values[name] = validated_value

    def __contains__(self, name):
        return name in self._values

    def __iter__(self):
        return iter(self._values)

    def __len__(self):
        return len(self._values)

    def _validate(self, name, value):
        """Pass `value` to `name`'s constructor and return the result"""
        return self._constructors[name](value)

    def add(self, name, constructor, default, description=None, getter=None, setter=None):
        """
        Add setting

        :param name: Identifier for this setting
        :param constructor: Callable that takes a value and converts it to the
            proper type
        :param default: Initial and default value
        :param description: Description of the setting for the user
        :param getter: Callable that takes no arguments and returns the
            setting's value or `None` to manage values in an internal
            :class:`dict`
        :param setter: Callable with one argument (`name` that sets the value or `None`
            to manage values in an internal :class:`dict`
        """
        self._constructors[name] = constructor
        self._descriptions[name] = description
        value = self._validate(name, default)
        self._values[name] = value
        self._defaults[name] = value
        if getter:
            self._getters[name] = getter
        if setter:
            self._setters[name] = setter

    def reset(self, name):
        """Reset setting to default/initial value"""
        self[name] = self._defaults[name]

    def default(self, name):
        """Return setting's default/initial value"""
        return self._defaults[name]

    def description(self, name):
        """Return setting's description"""
        return self._descriptions[name]
