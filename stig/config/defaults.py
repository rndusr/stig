import os

from xdg.BaseDirectory import xdg_config_home as XDG_CONFIG_HOME

from .. import __appname__, log, usertypes

RC_FILE = os.path.join(XDG_CONFIG_HOME, __appname__, 'rc')


def init(settings):
    settings.add(
        name='unit.transfer',
        constructor=usertypes.Option.partial(options=('bit', 'byte'), aliases={'b': 'bit', 'B': 'byte'}),
        default='byte',
        description='Transfer rate unit ("bit" or "byte")',
    )

    settings.add(
        name='unitprefix.transfer',
        constructor=usertypes.Option.partial(options=('decimal', 'binary'), aliases={'m': 'decimal', 'b': 'binary'}),
        default='decimal',
        description='Transfer rate unit prefix ("decimal" or "binary")',
    )

    settings.add(
        name='unit.storage',
        constructor=usertypes.Option.partial(options=('bit', 'byte'), aliases={'b': 'bit', 'B': 'byte'}),
        default='byte',
        description='Data storage unit ("bit" or "byte")',
    )

    settings.add(
        name='unitprefix.storage',
        constructor=usertypes.Option.partial(options=('decimal', 'binary'), aliases={'m': 'decimal', 'b': 'binary'}),
        default='binary',
        description='Data storage unit prefix ("decimal" or "binary")',
    )

    if log.debugging_is_enabled():
        log.debug('INITIAL SETTINGS:')
        for name, value in settings.items():
            log.debug('  %s = %r', name, value)
