from . import command, config, log

settings = config.Settings()

cmdmgr = command.CommandManager(
    info_handler=log.info,
    error_handler=log.error,
)

# clients = aiobtclientapi....?
