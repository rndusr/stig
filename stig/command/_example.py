import textwrap


class Example(str):
    """:class:`str` subclass that formats example commands with a description"""

    def __new__(cls, command, description='', description_gap=2):
        if not description:
            self = super().__new__(cls, command)
        else:
            text = f'{command}' + (' ' * description_gap) + f'# {description}'
            subsequent_indent = ' ' * (len(command) + description_gap) + '# '
            wrapped_text = textwrap.fill(
                text,
                subsequent_indent=subsequent_indent,
                width=90,
            )
            self = super().__new__(cls, wrapped_text)

        self._command = command
        self._description = description
        self._description_gap = description_gap
        return self

    @property
    def command(self):
        return self._command

    @property
    def description(self):
        return self._description

    @property
    def description_gap(self):
        return self._description_gap


class Examples(tuple):
    """
    Sequence of:class:`Example` instances

    The `description_gap` of each example is adjusted to align descriptions:

        rc /path/to/rc.example  # Read rc.example from absolute path
        rc ./rc.example         # Read rc.example from relative path
        rc rc.example           # Read rc.example from $XDG_CONFIG_HOME/.config/stig
                                # or current working directory
    """

    def __new__(cls, *examples):
        max_command_length = max(
            len(example.command)
            for example in examples
        )

        return super().__new__(cls, (
            Example(
                command=example.command,
                description=example.description,
                description_gap=max_command_length - len(example.command) + 2,
            )
            for example in examples
        ))
