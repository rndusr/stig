import shlex

from . import _error


class Argv(tuple):
    """Command name followed by zero or more arguments"""

    def __new__(cls, cmd):
        self = super().__new__(cls, (
            str(arg)
            for arg in cmd
        ))
        if len(self) < 1:
            raise _error.CommandError(f'Command cannot be empty: {cmd!r}')
        else:
            return self

    def __str__(self):
        return ' '.join(shlex.quote(arg) for arg in self)
