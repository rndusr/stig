import collections
import functools

from .. import log
from . import _argv, _cmd, _cmdchain, _constants, _error


class CommandManager:
    def __init__(self, *, info_handler, error_handler):
        self._info_handler = info_handler
        self._error_handler = error_handler
        self._commands = {}
        self._active_ui = None

    def register(self, cmdcls):
        """Add :class:`~.Command` subclass"""
        if not issubclass(cmdcls, _cmd.Command):
            raise RuntimeError(f'Not a Command subclass: {cmdcls!r}')

        # Initialize class (e.g. provide info/error message handlers, detect
        # `ui` attribute, translate `argspecs` into `argparse.ArgumentParser`
        # object, etc)
        cmdcls.setup(
            info_handler=self._info_handler,
            error_handler=self._error_handler,
        )

        # Store `cmdcls` for each of its supported interfaces
        if cmdcls.ui not in self._commands:
            self._commands[cmdcls.ui] = {}

        for name in cmdcls.names:
            self._commands[cmdcls.ui][name] = cmdcls

        log.debug('Registered %s command: %s: %s', cmdcls.ui, cmdcls.__name__, cmdcls.names)

    @property
    def available_uis(self):
        """
        Sequence of possible values for :attr:`active_ui`

        It contains deduplicated and sorted values of all :attr:`.Command.ui`
        values.
        """
        return tuple(sorted(self._commands))

    @property
    def active_ui(self):
        """
        Currently used user interface

        This determines which :class:`~.Commmand` subclasses are used to execute
        a command.

        User interfaces are specified by the :attr:`.Command.ui` attribute.
        """
        return self._active_ui

    @active_ui.setter
    def active_ui(self, ui):
        if isinstance(ui, collections.abc.Hashable):
            self._active_ui = ui
            # Invalidate cached_property
            try:
                del self.active_commands
            except AttributeError:
                pass
        else:
            raise ValueError(f'User interface is not hashable: {ui!r}')

    @functools.cached_property
    def active_commands(self):
        """
        Sequence of command classes for the :attr:`active_ui`

        :raise RuntimeError: if :attr:`active_ui` is unspecified (`None`)
        """
        if self.active_ui is None:
            raise RuntimeError('active_ui is not specified')
        else:
            return tuple(self._commands[self.active_ui].values())

    @functools.cached_property
    def all_commands(self):
        """Sequence of all command classes for all interfaces"""
        return tuple(
            cmdcls
            for ui_cmds in self._commands.values()
            for cmdcls in ui_cmds.values()
        )

    def get_cmdcls(self, cmdname, ui='ACTIVE'):
        """
        Get :class:`~.Command` subclass by :attr:`~.Command.name`

        If `ui` is "ACTIVE", return command class from :attr:`active_commands`.

        If `ui` is "ANY", return command class from :attr:`all_commands`.

        Otherwise, `ui` must be an existing user interface and only a command
        that supports it is returned.

        Returns None if no matching command class is registered.
        """
        if ui == 'ACTIVE':
            commands = self.active_commands
        elif ui == 'ANY':
            commands = self.all_commands
        else:
            try:
                commands = self._commands[ui].values()
            except KeyError:
                raise ValueError(f'Unknown interface: {ui!r}')

        for cmd in commands:
            if cmdname in cmd.names:
                return cmd

    async def run(self, command_chain, raise_exception=False):
        """
        Evaluate :class:`~.CommandChain`

        :return: `True` if command chain was evaluated successfully, `False`
            otherwise
        """
        assert isinstance(command_chain, _cmdchain.CommandChain), command_chain
        if not command_chain:
            return True  # No commands - no error

        # Iterate over Command instances
        log.debug('Running command chain: %r', command_chain)
        for process in self._each_process(command_chain):
            log.debug('Awaiting: %s', process)
            await process.execute(raise_exception=raise_exception)
            log.debug('Finished: %s', process)
            assert process.is_finished, process

        # Final process determines overall return value
        return process.success

    def _each_process(self, command_chain):
        previous_process = None

        def previous_process_ok():
            return (
                previous_process is None
                or previous_process.success
            )

        for item in command_chain:
            if (
                    (
                        item in _constants.OPERATORS_AND
                        and not previous_process_ok()
                    )
                    or (
                        item in _constants.OPERATORS_OR
                        and previous_process_ok()
                    )
            ):
                break
            elif item in _constants.OPERATORS:
                continue
            else:
                process = self._create_process(item)
                yield process
                previous_process = process

    def _create_process(self, argv):
        """Instantiate :class:`~.Command` subclass from :class:`~.Argv` object"""
        cmdname = argv[0]
        cmdcls = self.get_cmdcls(cmdname, ui='ACTIVE')
        if cmdcls is None:
            if self.get_cmdcls(cmdname, ui='ANY') is not None:
                # Command exists but not in active interface.  Store it in case
                # we want to run it later, e.g. `stig help keybindings` must
                # explicitly execute `bind` commands (which are TUI-only) before
                # it can show keybindings in CLI mode.  We run a dummy command
                # to not break a command chain.
                log.debug('Ignoring inactive command: %s', cmdname)
                self._ignored_calls.append(argv)
                return self._create_dummy_process(cmdname)
            else:
                return self._create_dummy_process(
                    cmdname=cmdname,
                    exception=_error.CommandError('No such command'),
                )
        else:
            return cmdcls(argv)

    def _create_dummy_process(self, cmdname, exception=None):
        """
        Return instance of custom :class:`~.Command` subclass

        This allows us to instantiate non-existing commands like any regular
        command.
        """

        class DummyCommand(_cmd.Command):
            name = cmdname
            ui = 'dummy-ui'
            category = 'dummies'
            description = 'Dummy command'

            def run(self):
                if exception:
                    raise exception

        DummyCommand.setup(
            info_handler=self._info_handler,
            error_handler=self._error_handler,
        )

        return DummyCommand(argv=_argv.Argv((cmdname,)))
