import re
import shlex

from . import _argv, _constants, _error


class CommandChain(tuple):
    """
    Sequence of :class:`~.Command` instances and :attr:`~.OPERATORS`
    """

    def __new__(cls, *cmdchain):
        self = super().__new__(cls, (
            item if item in _constants.OPERATORS else _argv.Argv(item)
            for item in cmdchain
        ))
        return self

    def __str__(self):
        return ' '.join(str(item) for item in self)

    _from_string_split_regex = re.compile(
        # Do not split at escaped operators
        r'(?<!\\)'
        # Join operators into `(\&|\||OR|AND|...)`
        + r'('
        + r'|'.join((
            re.escape(op)
            for op in _constants.OPERATORS
        ))
        + r')'
    )

    @classmethod
    def from_string(cls, string):
        """
        Shell-split `string` into arguments and pass the resulting sequence
        to :meth:`from_args`

        :raise ValueError: if `string` doesn't split (e.g. imbalanced quotes)
        """
        # Make sure any operator is surrounded by spaces so shlex.split() will
        # isolate them into separate arguments ("foo&bar|baz" -> "foo & bar |
        # baz" -> ["foo", "&", "bar", "|", "baz"])
        string_spaced = re.sub(
            cls._from_string_split_regex,
            r' \1 ',
            string,
        )

        # shlex.split() will raise ValueError for unbalanced quotes
        try:
            args = shlex.split(string_spaced)
        except ValueError as e:
            raise _error.CommandError(e)
        else:
            return cls.from_args(args)

    @classmethod
    def from_args(cls, args):
        """
        Separate `args` into :class:`Command` instances and
        :var:'~.commands.OPERATORS`

        :raise ValueError: if `args` is not a valid command chain
            (e.g. consecutive operators)
        """
        cmd = []
        cmdchain = []
        ops = set(_constants.OPERATORS)

        for arg in args:
            if arg in ops:
                # Append non-empty command
                if cmd:
                    cmdchain.append(_argv.Argv(cmd))
                    cmd.clear()

                if not cmdchain:
                    raise _error.CommandError(f'Command cannot start with operator: {arg}')
                elif cmdchain[-1] in ops:
                    raise _error.CommandError(f'Consecutive operators: {cmdchain[-1]} {arg}')
                else:
                    # Append operator
                    cmdchain.append(arg)

            elif arg and arg[0] == '\\' and arg[1:] in ops:
                # Append escaped operator to command
                cmd.append(arg[1:])

            else:
                # Append argument to command
                cmd.append(arg)

        # Append final command that wasn't appended because there is no trailing
        # operator
        if cmd:
            cmdchain.append(_argv.Argv(cmd))

        if cmdchain and cmdchain[-1] in ops:
            raise _error.CommandError(f'Command cannot end with operator: {arg}')

        return cls(*cmdchain)
