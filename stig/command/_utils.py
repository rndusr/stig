import collections


def args_as_sequence(args):
    """
    Return `args` as a sequence

    `args` may be a :class:`str` with comma-separated values or any kind of
    iterable.

    Each value from an iterable is also comma-separated.

    Empty arguments are excluded.
    """
    if not args:
        return ()

    elif isinstance(args, str):
        # Comma-separated values
        args = (
            str(arg).strip()
            for arg in args.split(',')
        )

    elif isinstance(args, collections.abc.Iterable):
        # Sequence or other iterable of comma-separated values
        args = (
            a
            for arg in args
            for a in args_as_sequence(arg)
        )

    else:
        # Anything else is a single value
        args = (str(args).strip(),)

    return tuple(
        arg
        for arg in args
        if arg
    )
