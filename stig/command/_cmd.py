import argparse
import functools
import inspect

from .. import log  # noqa: F401
from . import _argv, _error


class CommandArgumentParser(argparse.ArgumentParser):
    """Raise errors instead of printing them on stderr and exiting"""

    def exit(self, status=0, message=None):
        if message is not None:
            self.error(message)

    def error(self, message):
        raise _error.CommandError(str(message).capitalize())


class Command:
    """
    Base for all command classes

    :param argv: Command line as sequence (first item must always be the command name)
    """

    names = NotImplemented
    aliases = ()
    argspecs = ()
    usage = ''
    examples = ''

    # Handlers for info/error messages
    _info_handler = NotImplemented
    _error_handler = NotImplemented

    @classmethod
    def setup(cls, *, info_handler, error_handler):
        """
        Class initializer

        This class method MUST be called before this class can be used. This
        class method should not be called on classes of inactive user
        interfaces.
        """
        # Info and error message handlers
        cls._info_handler = info_handler
        cls._error_handler = error_handler

        # Autodetect `ui` attribute from caller
        for backsteps in range(1, 6):
            caller_frame = inspect.stack()[backsteps].frame
            caller_module_name = inspect.getmodule(caller_frame).__name__
            if '.ui.' in caller_module_name:
                cls.ui = caller_module_name.partition('.ui.')[-1].split('.')[0]
                break

        # 'names' attribute
        cls.names = (cls.name,) + tuple(cls.aliases)

        # Create argument parser
        cls._argparser = CommandArgumentParser(
            prog=cls.name,
            add_help=False,
        )
        for argspec in cls.argspecs:
            if 'names' not in argspec:
                raise RuntimeError(f'Missing "names" in argument spec: {argspec}')

            # Create a copy of argspec so we don't alter the original class
            # attribute and remove all items that CommandArgumentParser doesn't
            # understand
            argspec = argspec.copy()
            for k in (
                    'description',
                    'default_description',
                    'document_default_value',
            ):
                argspec.pop(k, None)

            # Translate string to argparse constant for convenience
            if 'nargs' in argspec and argspec['nargs'] == 'REMAINDER':
                argspec['nargs'] = argparse.REMAINDER

            # add_argument() expects argument names (e.g. ("--foo", "-f") as
            # positional arguments, but each `argspec` is a dictionary, so we
            # store these under the "names" key.
            argnames = argspec.pop('names', None)

            cls._argparser.add_argument(*argnames, **argspec)

    def info(self, msg):
        """Handle info message"""
        type(self)._info_handler(f'{self.name}: {msg}')

    def error(self, msg):
        """Handle error message"""
        type(self)._error_handler(f'{self.name}: {msg}')

    def __init__(self, argv):
        self._task = None
        self._success = None
        self._exception = None
        assert isinstance(argv, _argv.Argv), argv
        self._argv = argv

    def __repr__(self):
        argv_str = ', '.join(repr(arg) for arg in self.argv)
        if self.is_finished:
            state = f'success={self.success}'
        else:
            state = ' running'
        return (
            f'<{type(self).__name__}'
            f' [{self.ui}]'
            f' {state}'
            f' {argv_str}'
            '>'
        )

    @functools.cached_property
    def argv(self):
        """Complete command line"""
        return self._argv

    @functools.cached_property
    def command(self):
        """Name or alias of the command / first item in `argv`"""
        return self._argv[0]

    @functools.cached_property
    def args(self):
        """Arguments provided to :attr:`command`"""
        return self._argv[1:]

    @property
    def success(self):
        """
        Whether the command terminated successfully or `None` if it is still
        running
        """
        return self._success

    @property
    def is_finished(self):
        """Whether the command has terminated (regardless of :attr:`success`)"""
        return self._success is not None

    async def execute(self, raise_exception=False):
        try:
            kwargs = self._make_kwargs_for_run()
            await self.run(**kwargs)
        except _error.CommandError as e:
            self._success = False
            msg = str(e)
            if raise_exception:
                # Prepend command name to make debugging rc files and command chains easier
                raise _error.CommandError(f'{self.name}: {msg}' if msg else '')
            elif msg:
                # error() always prepends command name
                self.error(msg)
        else:
            self._success = True

    def _make_kwargs_for_run(self):
        # Create keyword arguments for run() method
        args_parsed = self._argparser.parse_args(self.args)
        kwargs = {}
        for argspec in self.argspecs:
            # First CLI name of the argument is the keyword argument for run()
            key = argspec['names'][0].lstrip('-').replace('-', '_')
            value = getattr(args_parsed, key)
            kwargs[key] = value
        return kwargs
