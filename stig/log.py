import inspect
import logging
import sys

# Should be the same as `__appname__`
_PROJECT_NAME = __name__[:__name__.index('.')]


def _get_logger():
    frameinfo = inspect.stack()[2]
    frame = frameinfo.frame
    module = inspect.getmodule(frame)
    return logging.getLogger(module.__name__)


def debugging_is_enabled():
    """
    Whether any logger in this application is enabled for logging.DEBUG or
    lower
    """
    for name, logger in logging.root.manager.loggerDict.items():
        if (
                name.startswith(_PROJECT_NAME)
                and isinstance(logger, logging.Logger)
                and logger.isEnabledFor(logging.DEBUG)
        ):
            return True
    return False


def debug(*args, **kwargs):
    # Bump stacklevel so "funcName", "lineno", etc point to this function's
    # caller, not to the caller of logger.debug() (i.e. this function).
    kwargs['stacklevel'] = 2
    _get_logger().debug(*args, **kwargs)


def info(*args, **kwargs):
    kwargs['stacklevel'] = 2
    _get_logger().info(*args, **kwargs)


def warning(*args, **kwargs):
    kwargs['stacklevel'] = 2
    _get_logger().warning(*args, **kwargs)


def error(*args, **kwargs):
    kwargs['stacklevel'] = 2
    _get_logger().error(*args, **kwargs)


def setup(*, debug_modules, debug_filepath=None):
    """
    Initialize logging

    :param debug_modules: Sequence of module names

        Module names may start with ".", which automatically prepends the
        project name.

    :param debug_filepath: If not `None`, log debug messages to this file instead
        of `sys.stderr`
    """

    class PerLevelFormatter(logging.Formatter):
        datefmt = '%H:%M:%S'
        formatters = {
            logging.DEBUG: logging.Formatter(
                # '%(asctime)s.%(msecs)03d: '
                '[%(name)s:%(funcName)s@%(lineno)s] '
                '%(message)s',
                datefmt=datefmt,
            ),
        }
        default_formatter = logging.Formatter('%(message)s', datefmt=datefmt)

        def format(self, record):
            formatter = self.formatters.get(record.levelno, self.default_formatter)
            return formatter.format(record)

    # Use custom logging.Formatter that uses different formatters for DEBUG/INFO
    formatter = PerLevelFormatter()
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    root_logger = logging.getLogger()
    root_logger.addHandler(stream_handler)

    # Show INFO messages from all modules
    root_logger.setLevel(logging.INFO)
    redirect_level('INFO', sys.stdout)

    # Show DEBUG messages only from specific modules
    if debug_modules:
        logging.getLogger(__name__).setLevel(logging.DEBUG)

        for module_name in debug_modules:
            if module_name[0] == '.':
                logger_name = f'{_PROJECT_NAME}{module_name}'
            else:
                logger_name = module_name
            logging.getLogger(logger_name).setLevel(logging.DEBUG)

    if debug_filepath is not None:
        file_handler = logging.FileHandler(debug_filepath)
        file_handler.setFormatter(formatter)
        root_logger.addHandler(file_handler)

    logging.getLogger(__name__).debug('Logging messages from %s to %s',
                                      ', '.join(debug_modules),
                                      debug_filepath or 'stderr')


def redirect_level(level, stream):
    """
    Redirect logging.LEVEL to `stream` (file-like object)

    .. warning:: Weird things will happen if :func:`redirect_level` is called
        before :func:`setup`.
    """
    root_logger = logging.getLogger()

    # Add filters to all existing handlers to NOT log the specified level
    for h in root_logger.handlers:
        h.addFilter(lambda record: record.levelname != level)

    # Add new `stream` handler that only logs the specified level
    stream_handler = logging.StreamHandler(stream)
    stream_handler.addFilter(lambda record: record.levelname == level)
    root_logger.addHandler(stream_handler)


def add_handler(handler):
    """Add :class:`logging.Handler` to root logger"""
    root_logger = logging.getLogger()
    root_logger.addHandler(handler)
    logging.getLogger(__name__).debug('Started logging to %r', handler)


def remove_handler(handler):
    """Remove :class:`logging.Handler` from root logger"""
    root_logger = logging.getLogger()
    root_logger.removeHandler(handler)
    logging.getLogger(__name__).debug('Stopped logging to %r', handler)


_stdouterr_handlers = []

def disable_stdouterr():
    """Stop logging to stdout and/or stderr"""
    if _stdouterr_handlers:
        raise RuntimeError('Looks like you already called disable_stdouterr()')

    root_logger = logging.getLogger()
    for handler in tuple(root_logger.handlers):
        stream = getattr(handler, 'stream', None)
        if stream and stream.name in ('<stderr>', '<stdout>'):
            _stdouterr_handlers.append(handler)
            root_logger.removeHandler(handler)

    logging.getLogger(__name__).debug('Disabled logging to stdout/stderr')


def enable_stdouterr():
    """Resume logging to stdout and/or stderr"""
    root_logger = logging.getLogger()
    for handler in tuple(_stdouterr_handlers):
        root_logger.addHandler(handler)
        _stdouterr_handlers.remove(handler)

    logging.getLogger(__name__).debug('Enabled logging to stdout/stderr')
