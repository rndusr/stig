import asyncio
import sys

from . import cliargs, command, config, log, objects, ui


def run():
    # Read CLI options and subcommands
    clicfg, cli_command_chain = cliargs.parse(sys.argv[1:])

    # Initialize logging
    log.setup(debug_modules=clicfg['debug'], debug_filepath=clicfg['debug_file'])
    if log.debugging_is_enabled():
        log.debug('CLI OPTIONS:')
        for name, value in clicfg.items():
            log.debug('  %s: %r', name, value)

        log.debug('CLI COMMANDS:')
        for cmd in cli_command_chain:
            log.debug('  %r', cmd)

    # Initialize settings
    config.defaults.init(objects.settings)

    # Load user interface
    ui_module = ui.load(name=(
        clicfg['ui']
        or
        ui.guess(cli_command_chain)
    ))

    # Sequence of initializing commands (not a command chain)
    initial_commands = []
    if not clicfg['no_default_rc_file']:
        initial_commands.append(
            command.CommandChain.from_args(('rc', config.defaults.RC_FILE))
        )

    # Execute user interface and return it's result (usually exit code)
    return asyncio.run(
        ui_module.run(
            startup_commands=_get_startup_commands_coroutine(
                initial_commands=initial_commands,
                cli_command_chain=cli_command_chain,
            ),
        ),
    )


def _get_startup_commands_coroutine(*, initial_commands, cli_command_chain):

    async def _startup_commands():
        # Initial commands are executing commands from rc file and stuff like that
        for cmd in initial_commands:
            success = await objects.cmdmgr.run(cmd)
            if not success:
                return False

        # CLI terminates after subcommands are executed
        if cli_command_chain:
            log.debug('CLI commands:')
            for cmd in cli_command_chain:
                log.debug('  :%s', cmd)
        return await objects.cmdmgr.run(cli_command_chain)

    return _startup_commands
