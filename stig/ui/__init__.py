import argparse
import importlib
import inspect
import re
import sys
import types

from . import cli, tui, tui2


def ui_names():
    """Return sequence of valid arguments for :func:`load`"""
    def is_ui_module(obj):
        return (
            isinstance(obj, types.ModuleType)
            and re.search(r'^stig\.ui\.[a-z_0-9]+$', obj.__name__)
        )

    ui_modules = inspect.getmembers(sys.modules[__name__], predicate=is_ui_module)
    return tuple(
        name
        for name, module in ui_modules
    )


def load(name=None):
    """Import and return user interface implementation (submodule)"""
    package = __name__
    path = f'.{name}'
    try:
        return importlib.import_module(path, package=package)
    except ModuleNotFoundError:
        raise ValueError(f'No such user interface: {name}')


def guess(subcmds):
    """Guess and :func:`load` user interface based on CLI subcommands"""
    # TODO: Do what the docstring says
    return ui_names()[0]
