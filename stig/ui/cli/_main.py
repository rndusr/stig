from ... import command, log, objects, utils


def _init():
    log.debug('Initializing CLI')

    # Load CLI commands
    from . import _commands
    for cmdcls in utils.find_subclasses(
            basecls=command.Command,
            module_names=(_commands.__name__,),
    ):
        objects.cmdmgr.register(cmdcls)

    # Tell CommandManager which Command subclasses to use by default
    objects.cmdmgr.active_ui = 'cli'


async def run(*, startup_commands):
    _init()
    success = await startup_commands()
    return 0 if success else 1
