import os

from .... import __appname__, command, config, log, objects, utils


class Rc(command.Command):
    name = 'rc'
    aliases = ('source',)
    category = 'configuration'
    description = 'Run commands in rc file'
    usage = ('rc <FILE>',)
    examples = command.Examples(
        command.Example(
            command=f'rc {os.sep}path{os.sep}to{os.sep}rc.example',
            description='Read rc.example from absolute path',
        ),
        command.Example(
            command=f'rc .{os.sep}rc.example',
            description='Read rc.example from relative path',
        ),
        command.Example(
            command='rc rc.example',
            description=(
                f'Read rc.example from $XDG_CONFIG_HOME{os.sep}.config{os.sep}{__appname__} '
                'or current working directory'
            ),
        ),
    )
    argspecs = (
        {
            'names': ('FILE',),
            'description': (
                'Path to rc file\n'
                f'If FILE does not exist and does not start with "{os.sep}", ".{os.sep}" or "~", '
                f'"$XDG_CONFIG_HOME{os.sep}.config{os.sep}{{__appname__}}{os.sep}" '
                'is prepended.'
            ),
        },
    )

    async def run(self, FILE):
        filepath = self.get_rc_filepath(FILE)
        try:
            lines = config.rcfile.read(filepath)
        except config.rcfile.RCFileError as e:
            raise command.CommandError(f'Failed to read rc file: {filepath}: {e}')
        else:
            log.debug('Running commands from rc file: %r', filepath)
            for cmdline in lines:
                try:
                    await objects.cmdmgr.run(
                        cmdline,
                        raise_exception=True,
                    )
                except NotImplementedError:
                    # Command is not implemented for active user interface
                    pass
                except command.CommandError as e:
                    pretty_filepath = utils.tildify_path(filepath)
                    raise command.CommandError(f'{pretty_filepath}@{cmdline.number}: {e}')

    @staticmethod
    def get_rc_filepath(path):
        """Get computed RC file path as documented in `argspecs`"""
        path = os.path.expanduser(path)
        if (
                not os.path.exists(path)
                and not os.path.isabs(path)
                and not path.startswith(f'.{os.sep}')
                and not path.startswith('~')
        ):
            default_dir = os.path.dirname(config.defaults.RC_FILE)
            path = os.path.join(default_dir, path)
        return path
