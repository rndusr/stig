import subprocess

from .... import command, log, objects


class Set(command.Command):
    name = 'set'
    category = 'configuration'
    description = 'Change or show configuration'

    usage = (
        'set [<NAME>[:eval]] [<VALUE>]',
    )

    examples = (
        'set                     # Display all settings and values',
        'set unit.transfer       # Display unit for transfer rates',
        'set unit.transfer byte  # Change unit for transfer rates',

        # 'set connect.host my.server.example.org',
        # 'set connect.user jonny_sixpack',
        # 'set connect.password:eval getpw --id transmission',
        # 'set tui.log.height +=10',
    )

    argspecs = (
        {
            'names': ('NAME',),
            'nargs': '?',
            'description': (
                'Setting name'
                + (
                    '\nMay be a partial name to show multiple settings'
                    'if VALUE is not provided.'
                )
                + (
                    '\nAppend ":eval" to evaluate VALUE as a '
                    'shell command and use its output as the value.'
                )
            ),
        },

        {
            'names': ('VALUE',),
            'nargs': 'REMAINDER',
            'description': (
                'New value or shell command that prints the new value to stdout'
                '\nNumerical values can be adjusted by prepending "+=" or "-=".'
            ),
        },
    )

    async def run(self, NAME, VALUE):
        if not NAME:
            # Display all settings
            names = self._find_settings()
            self.display_settings(names)

        else:
            if not VALUE:
                # Display settings that match NAME
                names = self._find_settings(NAME)
                if names:
                    self.display_settings(names)
                else:
                    raise command.CommandError(f'No matching settings: {NAME}')

            else:
                # Change setting
                log.debug('Setting %s = %r', NAME, VALUE)
                name = self._parse_name(NAME)
                value = self._parse_value(
                    value=VALUE,
                    as_list=isinstance(objects.settings[name], (tuple, list)),
                    is_command=NAME.endswith(':eval'),
                )
                self._change_setting(name, value)
                self.setting_changed(name)

    def _find_settings(self, partial_name=None):
        if partial_name:
            return tuple(
                name
                for name in objects.settings
                if partial_name in name
            )
        else:
            return tuple(objects.settings)

    def _parse_name(self, name):
        if name.endswith(':eval'):
            name = name[:-5]
        if name in objects.settings:
            return name
        else:
            raise command.CommandError(f'No such setting: {name}')

    def _parse_value(self, value, as_list=False, is_command=False):
        if is_command:
            value = [self._eval_cmd(value)]
        if as_list:
            return command.args_as_sequence(value)
        else:
            return ' '.join(value)

    def _eval_cmd(self, cmd):
        log.debug('Running shell command: %r', cmd)
        proc = subprocess.run(
            cmd,
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
        )
        stderr = proc.stderr.strip()
        if stderr:
            raise command.CommandErrorError(stderr)
        else:
            return proc.stdout.strip()

    def _change_setting(self, name, value):
        try:
            objects.settings[name] = value
        except ValueError as e:
            raise command.CommandError(f'{name}: {e}: {value}')
