from ... import command, log, objects, utils


def _init():
    log.debug('Initializing TUI2')

    # Load TUI commands
    from . import _commands
    for cmdcls in utils.find_subclasses(
            basecls=command.Command,
            module_names=(_commands.__name__,),
    ):
        objects.cmdmgr.register(cmdcls)

    # Tell CommandManager which Command subclasses to use by default
    objects.cmdmgr.active_ui = 'tui2'


async def run(startup_commands):
    _init()

    from . import _app
    app = _app.Application()
    return await app.run(startup_commands=startup_commands)
