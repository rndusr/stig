import asyncio
import functools
import types

import prompt_toolkit
from prompt_toolkit.layout.containers import HSplit, Window
from prompt_toolkit.layout.controls import FormattedTextControl
from prompt_toolkit.layout.layout import Layout

from ... import log
from . import _key, _widgets


class Application:
    ...

    def __init__(self):
        # Don't mess up our precious TUI
        log.disable_stdouterr()

        # Handle any unforseen exceptions
        self._loop = asyncio.get_event_loop()
        self._loop.set_exception_handler(self._handle_exception)

        keybindings = prompt_toolkit.key_binding.KeyBindings()

        @keybindings.add('q', eager=True)
        def _(event):
            event.app.exit(result=0)

        @keybindings.add('<any>')
        def _(event):
            key = _key.Key.from_ptk_value(event.key_sequence[0].key)
            log.debug('Pressed any key: %r -> %r', event.key_sequence, key)

        self._app = prompt_toolkit.Application(
            full_screen=True,
            mouse_support=False,
            layout=self.layout,
            key_bindings=keybindings,
        )

    async def run(self, startup_commands):
        try:
            return await self._app.run_async(
                set_exception_handler=False,
                pre_run=self._setup_factory(startup_commands),
            )
        finally:
            log.enable_stdouterr()

    def _setup_factory(self, startup_commands):
        def setup():
            # Run RC and CLI commands concurrently
            log.debug('Awaiting %s', startup_commands)
            self._app.create_background_task(startup_commands())

        return setup

    def _handle_exception(self, loop, context):
        exception = context.get('exception')
        if exception:
            log.debug('Handling uncaught exception: %r', exception)
            self._app.exit(exception=exception)

    @functools.cached_property
    def widgets(self):
        return types.SimpleNamespace(
            header=_widgets.Header(self),
            footer=_widgets.Footer(self),
            tabs=Window(
                content=FormattedTextControl(text='<tabs>'),
                dont_extend_height=False,
                dont_extend_width=False,
            ),
        )

    @functools.cached_property
    def layout(self):
        return Layout(
            HSplit([
                self.widgets.header,
                self.widgets.tabs,
                self.widgets.footer,
            ]),
        )

    def invalidate(self):
        self._app.invalidate()
