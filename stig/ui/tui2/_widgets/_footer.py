from prompt_toolkit.layout.containers import VSplit

from . import _base


class Footer(_base.WidgetBase):
    """Multiple widgets at the bottom"""

    def __pt_container__(self):
        return VSplit([
            self.connections,
            self.counts,
        ])
