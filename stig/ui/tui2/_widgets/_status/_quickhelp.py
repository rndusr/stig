from . import _base


class QuickHelp(_base.StatusBase):
    """Number of total/isotlated/stopped/etc torrents"""

    def setup(self):
        self.text = '<quickhelp>'
