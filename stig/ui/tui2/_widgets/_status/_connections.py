from . import _base


class Connections(_base.StatusBase):
    """
    Connection status for clients

    Client status is very compact by default but can be expanded to a drop down menu.
    """

    def setup(self):
        self.text = '<connections>'

        import asyncio
        asyncio.get_event_loop().call_later(1, self.arf)

    def arf(self):
        self.text = 'arf!'
        self.app.invalidate()
