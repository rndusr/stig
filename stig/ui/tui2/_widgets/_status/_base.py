from prompt_toolkit.layout.containers import Window
from prompt_toolkit.layout.controls import FormattedTextControl


class StatusBase:
    """Base class for header/footer status widgets"""

    def __init__(self, app):
        self._app = app
        self.setup()

    @property
    def app(self):
        return self._app

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, text):
        self._text = text

    def __pt_container__(self):
        return Window(
            content=FormattedTextControl(lambda: self.text),
            dont_extend_height=True,
            dont_extend_width=True,
        )
