from . import _base


class Counts(_base.StatusBase):
    """Number of total/isotlated/stopped/etc torrents"""

    def setup(self):
        self.text = '<counts>'
