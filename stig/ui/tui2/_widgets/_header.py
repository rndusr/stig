from prompt_toolkit.layout.containers import VSplit

from . import _base


class Header(_base.WidgetBase):
    """Multiple widgets at the top"""

    def __pt_container__(self):
        return VSplit([
            self.connections,
            self.spacer,
            self.counts,
            self.spacer,
            self.quickhelp,
        ])
