import functools

from prompt_toolkit.layout.containers import Window
from prompt_toolkit.layout.controls import FormattedTextControl

from . import _status


class WidgetBase:
    """Base class for the main widgets"""

    def __init__(self, app):
        self._app = app

    @property
    def app(self):
        """:class:`~._app.Application` instance from instantiation"""
        return self._app

    @functools.cached_property
    def connections(self):
        return _status.Connections(self.app)

    @functools.cached_property
    def counts(self):
        return _status.Counts(self.app)

    @functools.cached_property
    def quickhelp(self):
        return _status.QuickHelp(self.app)

    @functools.cached_property
    def spacer(self):
        return Window(
            FormattedTextControl(' '),
            dont_extend_width=False,
            dont_extend_height=True,
        )
