import functools

import prompt_toolkit as ptk


class Key(str):
    """Key or key combination as string"""

    _MODIFIERS = ('Alt', 'Ctrl', 'Shift')
    _FUNCTION_KEYS = (
        'F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'F10', 'F11', 'F12',
        'F13', 'F14', 'F15', 'F16', 'F17', 'F18', 'F19', 'F20', 'F21', 'F22', 'F23', 'F24',
    )
    _KEY_NAMES = (
        'Enter', 'Escape', 'Tab', 'Space', 'Backspace',
        'Left', 'Right', 'Up', 'Down',
        'Insert', 'Delete', 'Home', 'End',
        'PageUp', 'PageDown',
    )
    _SPECIAL_KEYS = _KEY_NAMES + _FUNCTION_KEYS

    _COMBINATION_TRANSLATION = {
        # Map `(KEY_NAME, MODIFIERS)` to `(NEW_KEY_NAME, NEW_MODIFIERS)`
        ('@', ('Ctrl',)): ('Space', ('Ctrl',)),
        ('i', ('Ctrl',)): ('Tab', (),),
        ('h', ('Ctrl',)): ('Backspace', (),),
        ('m', ('Ctrl',)): ('Enter', (),),
    }

    def __new__(cls, key, modifiers=(), validate=True):
        if not key:
            raise ValueError('Key must not be empty')

        # Some control sequences have multiple combinations and ptk seems to
        # prefer the more obscure representation
        try:
            key, modifiers = cls._COMBINATION_TRANSLATION[(key, tuple(modifiers))]
        except KeyError:
            pass

        # Validate key
        if (
                # Even exotic keyboard layouts should produce a single grapheme
                # per keypress, which should be what len() measures
                len(key) > 1
                and key not in cls._SPECIAL_KEYS
        ):
            raise ValueError(f'Invalid key: {key}')

        # Validate modifiers
        if modifiers:
            modifiers = tuple(sorted(modifiers))
            for modifier in modifiers:
                if modifier not in cls._MODIFIERS:
                    raise ValueError(f'Invalid modifier: {modifier}')
                elif modifiers.count(modifier) > 1:
                    raise ValueError(f'Duplicate modifier: {modifier}')

            string = ''.join((
                '-'.join(modifiers),
                '-',
                key,
            ))
        else:
            string = key
            modifiers = ()

        self = super().__new__(cls, string)
        self._key = key
        self._modifiers = modifiers

        # Check if key combination is actually supported
        if validate:
            self._validate()

        return self

    def _validate(self):
        # No need to check single grapheme keys with no modifiers or only Alt.
        # As far as ptk is concerned, Alt is just a preceding Escape byte.
        if self.modifiers and self.modifiers != ('Alt',):
            if self.as_ptk_value not in ptk.input.ansi_escape_sequences.REVERSE_ANSI_SEQUENCES:
                raise ValueError(f'Unsupported key combination: {self}')

    # @classmethod
    # def from_string(cls, key):
    #     TODO

    @property
    def key(self):
        return self._key

    @property
    def modifiers(self):
        return self._modifiers

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return super().__eq__(other)
        else:
            return NotImplemented

    def __hash__(self):
        return super().__hash__()

    def __str__(self):
        return f'<{super().__str__()}>'

    def __repr__(self):
        return (
            f'{type(self).__name__}('
            f'{self.key!r}'
            f', modifiers={self.modifiers!r}'
            ')'
        )

    _PTK_MODIFIERS_TRANSLATION = {
        'c': 'Ctrl',
        's': 'Shift',
    }
    _PTK_MODIFIERS_TRANSLATION_REVERSED = {
        our_name: ptk_name
        for ptk_name, our_name in _PTK_MODIFIERS_TRANSLATION.items()
    }

    _PTK_KEY_NAME_TRANSLATION = {
        ' ': 'Space',
        'pageup': 'PageUp',
        'pagedown': 'PageDown',
    }
    _PTK_KEY_NAME_TRANSLATION_REVERSED = {
        our_name: ptk_name
        for ptk_name, our_name in _PTK_KEY_NAME_TRANSLATION.items()
    }

    _PTK_COMBINATION_TRANSLATION_REVERSED = {
        our_name: ptk_name
        for ptk_name, our_name in _COMBINATION_TRANSLATION.items()
    }

    @classmethod
    def from_ptk_value(cls, key_value):
        """
        Get instances from :class:`prompt_toolkit.keys.Keys.value`
        """
        def fix_key_name(key_value):
            try:
                return cls._PTK_KEY_NAME_TRANSLATION[key_value]
            except KeyError:
                if len(key_value) > 1:
                    # Named key (e.g. "enter", "insert", etc)
                    return key_value.capitalize()
                else:
                    return key_value

        if '-' in key_value and len(key_value) > 1:
            # Split "s-c-foo" into ["s", "c", "foo"]
            parts = key_value.split('-')

            # Last part is the actual key
            key_name = fix_key_name(parts.pop(-1))

            # Everything before the last part is a modifier
            modifiers = set()
            for part in parts:
                modifiers.add(cls._PTK_MODIFIERS_TRANSLATION[part])

        else:
            # No modifiers
            key_name = fix_key_name(key_value)
            modifiers = ()

        self = cls(key_name, modifiers=modifiers, validate=False)
        return self

    @functools.cached_property
    def as_ptk_value(self):
        """
        Translate to python-prompt-toolkit name

        Example:

            <Ctrl-Shift-PageUp> -> c-s-pageup
        """
        modifiers_without_alt = tuple(
            modifier
            for modifier in self.modifiers
            if modifier != 'Alt'
        )
        key, modifiers = self._PTK_COMBINATION_TRANSLATION_REVERSED.get(
            (self.key, modifiers_without_alt),
            (self.key, self.modifiers),
        )

        try:
            ptk_key = self._PTK_KEY_NAME_TRANSLATION_REVERSED[key]
        except KeyError:
            if len(key) > 1:
                # Named key (e.g. "enter", "insert", etc)
                ptk_key = key.lower()
            else:
                # Preserve case for letters (e.g. "x" / "X")
                ptk_key = key

        if modifiers:
            # Alt is handled as preceding Escape byte by ptk. It is not part of
            # ptk Key sequences.
            ptk_modifiers = '-'.join(
                self._PTK_MODIFIERS_TRANSLATION_REVERSED[modifier]
                for modifier in modifiers
                if modifier != 'Alt'
            )
            return f'{ptk_modifiers}-{ptk_key}'
        else:
            return ptk_key
