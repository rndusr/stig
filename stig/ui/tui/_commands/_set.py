from .... import log, objects
from ... import _common


class Set(_common.commands.Set):
    def display_settings(self, names):
        for name in names:
            self.info(f'{name} = {objects.settings[name]}')

    def setting_changed(self, name):
        log.debug('Setting changed: %s = %r', name, objects.settings[name])
        print('>>>', name, '=', objects.settings[name])
