import textual.containers
import textual.widget
import textual.widgets

# https://textual.textualize.io/guide/widgets/#line-api


class TorrentList(
        textual.widget.Widget,
        # can_focus=True,
        can_focus_children=True,
):
    """..."""

    def __init__(self):
        super().__init__(
            classes='torrentlist',
        )

    def compose(self):

        yield Torrent(id='t1', name='My\nTorrent')
        yield Torrent(id='t2', name='Your Torrent')
        yield Torrent(id='t3', name='Some\nother\nTorrent')


class Torrent(textual.widgets.Static, can_focus=True):
    """..."""

    def __init__(self, id, name):
        super().__init__(
            name,
            id=id,
            classes='torrent',
        )
