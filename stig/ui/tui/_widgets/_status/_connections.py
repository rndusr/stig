from . import _base


class Connections(_base.Status):
    """
    Connection status for clients

    Client status is very compact by default but can be expanded to a drop down menu.
    """

    def __init__(self):
        super().__init__(id='status-connections')

    # https://textual.textualize.io/widgets/content_switcher/
