import textual.containers
import textual.widgets


class Status(textual.widgets.Static):
    """Base class for status info widgets"""

    def __init__(self, id, text=None):
        if text is None:
            super().__init__(f'<{type(self).__name__} placeholder>', id=id)
        else:
            super().__init__(text, id=id)
