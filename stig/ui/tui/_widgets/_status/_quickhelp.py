from . import _base


class QuickHelp(_base.Status):
    """Display some keybindings for essential commands like :quit"""

    def __init__(self):
        super().__init__(id='status-quickhelp')
