from . import _base


class Counts(_base.Status):
    """Number of total/isotlated/stopped/etc torrents"""

    def __init__(self):
        super().__init__(id='status-counts')
