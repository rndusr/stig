import pkgutil

import textual.app
import textual.logging
import textual.widgets

from .... import __appname__, log


class Application(textual.app.App):

    CSS = pkgutil.get_data(f'{__appname__}.ui', 'default.css').decode('utf8')

    async def run_async(self, *args, startup_commands, **kwargs):
        # Log regular logging messages to textual's internal logger so they can
        # be viewed in another terminal by running `textual console`
        log.add_handler(textual.logging.TextualHandler())
        log.disable_stdouterr()

        # Store startup commands so they can be awaited on_ready()
        self._startup_commands = startup_commands

        return await super().run_async(*args, **kwargs)

    def on_ready(self):
        self.run_worker(self._startup_commands(), name='startup_commands')
        log.debug('Awaiting %s', self._startup_commands)
        delattr(self, '_startup_commands')

        for _ in range(30):
            self.log_message()
        # self.set_interval(1, self.log_message)

    lorem = 'So many resources are devoted to internal issues that no external input can be processed anymore, and the system stops working. The world may be undergoing a revolution, Rome may be burning, but the philosophical discourse remains detached, meaningless, and utterly oblivious. Time for an upgrade.'

    def _count(self):
        log.info('counter: %r', self.i)
        type(self).i += 1

    def log_message(self):
        import random
        rand = random.randint(30, 400)

        if rand % 2 == 0:
            level = 'info'
        elif rand % 3 == 0:
            level = 'warning'
        elif rand % 5 == 0:
            level = 'error'
        else:
            level = 'debug'

        msg = self.lorem[:rand]
        getattr(log, level)(msg)

    def compose(self):
        from .. import _tuiobjects

        main = textual.widgets.Placeholder('main area')
        main.styles.height = '1fr'

        from . import _torrentlist
        main = _torrentlist.TorrentList()

        yield _tuiobjects.header
        yield main
        yield _tuiobjects.logger
        yield _tuiobjects.footer

    def on_key(self, event):
        if event.key == 'd':
            self.dark = not self.dark
        elif event.key == 'q':
            self.exit()
        elif event.key == 'l':
            self.log_message()
        elif event.key in ('k', 'up'):
            from .. import _tuiobjects
            _tuiobjects.logger.scroll_up(animate=False)
        elif event.key in ('j', 'down'):
            from .. import _tuiobjects
            _tuiobjects.logger.scroll_down(animate=False)
        # TODO: textual doesn't seem to support alt bindings. This is a
        #       dealbreaker. Maybe this iss fixed in the future?
        elif event.key in ('alt+v', 'pageup'):
            from .. import _tuiobjects
            _tuiobjects.logger.scroll_page_up(animate=False)
        elif event.key in ('ctrl+v', 'pagedown'):
            from .. import _tuiobjects
            _tuiobjects.logger.scroll_page_down(animate=False)
        else:
            log.debug('Key pressed: %r', event)
