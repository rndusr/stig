import textual.containers
import textual.widget
import textual.widgets

from . import _status


class Header(textual.widgets.Static):
    """Status information at the top"""

    def __init__(self):
        super().__init__(
            id='header',
        )

        self.styles.height = 'auto'

        # Contained widgets
        self.connections = _status.Connections()
        self.quickhelp = _status.QuickHelp()
        self.counts = _status.Counts()

    def compose(self):
        yield _status.EvenlySpaced(
            self.connections,
            self.quickhelp,
            self.counts,
            layout='horizontal',
        )
