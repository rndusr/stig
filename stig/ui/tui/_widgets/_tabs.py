import textual.widget


class Tabs(textual.widget.Widget, can_focus=False):
    """Main area that contains of one or more tabs"""

    def __init__(self):
        super().__init__(
            id='tabs',
        )

    def compose(self):
        yield textual.widgets.Static('<tabs placeholder>')
