import logging
import time

import textual.widgets

from .... import log


class TUILogRecordHandler(logging.Handler):
    """Feed :class:`~.logging.LogRecord` instances to a :class:`Log`"""

    def __init__(self, logwidget):
        super().__init__()
        self._logwidget = logwidget
        self.setLevel(logging.DEBUG)

    def emit(self, record):
        self._logwidget.add(record)


class Log(textual.widget.Widget):
    """Info and error messages"""

    def __init__(self):
        super().__init__(id='log')

        self._handler = TUILogRecordHandler(self)

        self.styles.height = 'auto'
        self.styles.min_height = 0
        self.styles.max_height = 10
        self.styles.overflow_x = 'hidden'  # Disable horizontal scrollbar
        self.styles.overflow_y = 'auto'    # Display vertical scrollbar if needed

    def on_mount(self):
        log.add_handler(self._handler)

    def on_resize(self, event):
        max_offset = self.virtual_size.height - self.container_size.height
        print('SCROLLING:', self.scroll_offset, 'size:', self.size,
              'virtual size:', self.virtual_size, 'container size:', self.container_size,
              'max_offset:', max_offset)
        if self.scroll_offset.y == max_offset:
            self.scroll_end(animate=False)

    def __del__(self):
        log.remove_handler(self._handler)

    i = 0

    def add(self, record):
        message = record.getMessage()
        if record.levelno >= logging.ERROR:
            level = 'error'
        elif record.levelno >= logging.WARNING:
            level = 'warning'
        elif record.levelno >= logging.INFO:
            level = 'info'
        else:
            level = 'debug'

        self.mount(LogEntry(f'<{self.i}> {message}', level))
        type(self).i += 1

        max_offset = self.virtual_size.height - self.container_size.height
        print('SCROLLING:', self.scroll_offset, 'size:', self.size,
              'virtual size:', self.virtual_size, 'container size:', self.container_size,
              'max_offset:', max_offset)
        if self.scroll_offset.y == max_offset:
            self.scroll_end(animate=False)


class LogEntry(textual.widget.Widget):
    def __init__(self, message, level):
        super().__init__(classes='log-entry')

        assert level in ('error', 'warning', 'info', 'debug'), level
        self._message = message
        self._level = level
        self._dupes = 0

        self._widgets = {
            'timestamp': LogEntryTimestamp(),
            'message': LogEntryMessage(message, level),
        }

        self.styles.layout = 'horizontal'
        self.styles.width = '100%'
        self.styles.height = 'auto'

    def compose(self):
        yield self._widgets['timestamp']
        yield self._widgets['message']

    # def render(self, *args, **kwargs):
    #     print('############### render(%r, %r, %r)', self, args, kwargs)
    #     return super().render(*args, **kwargs)

    # def render_line(self, *args, **kwargs):
    #     print('############### render_line(%r, %r, %r)', self, args, kwargs)
    #     return super().render_line(*args, **kwargs)

    @property
    def message(self):
        return self._message

    @property
    def level(self):
        return self._level

    def __repr__(self):
        return f'{type(self).__name__}({self.message!r}, level={self.level!r})'


class LogEntryTimestamp(textual.widgets.Static):
    def __init__(self):
        super().__init__(time.strftime('%H:%M:%S'), classes='log-timestamp')
        self.styles.width = 'auto'
        self.styles.height = '100%'


class LogEntryMessage(textual.widgets.Static):
    def __init__(self, message, level):
        super().__init__(message, classes=f'log-message log-message-{level}')
        self.styles.width = '100%'
        self.styles.height = 'auto'
