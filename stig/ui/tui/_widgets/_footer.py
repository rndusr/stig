import textual.widget


class Footer(textual.widget.Widget, can_focus=False):
    """Status information at the bottom"""

    def __init__(self):
        super().__init__(
            id='footer',
        )

        self.styles.height = 'auto'

    def compose(self):
        yield textual.widgets.Static('<torrent counts placeholder>')
        yield textual.widgets.Static('<bandwidth usage placeholder>')
