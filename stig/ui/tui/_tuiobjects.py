from . import _widgets

app = _widgets.Application()

header = _widgets.Header()
tabs = _widgets.Tabs()
logger = _widgets.Log()
footer = _widgets.Footer()
