from ... import command, log, objects, utils


def _init():
    log.debug('Initializing TUI')

    # Load TUI commands
    from . import _commands
    for cmdcls in utils.find_subclasses(
            basecls=command.Command,
            module_names=(_commands.__name__,),
    ):
        objects.cmdmgr.register(cmdcls)

    # Tell CommandManager which Command subclasses to use by default
    objects.cmdmgr.active_ui = 'tui'


async def run(startup_commands):
    _init()

    # TODO: Open some default tabs unless any of the `initial_commands` or
    #       `cli_command_chain` have already done that.

    from . import _tuiobjects
    return await _tuiobjects.app.run_async(startup_commands=startup_commands)
